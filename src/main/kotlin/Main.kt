import java.net.NetworkInterface
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import net.mm2d.upnp.ControlPoint
import net.mm2d.upnp.ControlPointFactory
import net.mm2d.upnp.Device

fun main() {
    val scope = CoroutineScope(Dispatchers.IO)
    val cp = ControlPointFactory.create(
        interfaces = listOf(NetworkInterface.getByName("enp5s0"))
    )
    val updates = mutableMapOf<String, MutableStateFlow<Boolean>>()
    cp.addDiscoveryListener(object : ControlPoint.DiscoveryListener {
        override fun onDiscover(device: Device) {
            if (device.deviceType == "urn:panasonic-com:device:p00RemoteController:1") {
                val svc = device.findServiceById("urn:upnp-org:serviceId:p00NetworkControl")!!
                val id = getPanasonicId(device)
                val flow: MutableStateFlow<Boolean>
                val nFlow = updates[id]
                println("Net: ${device.udn} ($nFlow)")
                if (nFlow == null) {
                    val isOnline = cp.deviceList.any {
                        it.deviceType == "urn:panasonic-com:device:p00ProAVController:1" && getPanasonicId(it) == id
                    }
                    flow = MutableStateFlow(isOnline)
                    updates[id] = flow
                } else {
                    flow = nFlow
                }
                scope.launch {
                    delay(100)
                    processPanasonic(svc, flow.asStateFlow())
                }
            } else if (device.deviceType == "urn:panasonic-com:device:p00ProAVController:1") {
                val id = getPanasonicId(device)
                val flow = updates[id]
                if (flow == null) {
                    updates[id] = MutableStateFlow(true)
                } else {
                    scope.launch {
                        flow.emit(true)
                    }
                }
            }
        }

        override fun onLost(device: Device) {
            if (device.deviceType == "urn:panasonic-com:device:p00ProAVController:1") {
                val id = getPanasonicId(device)
                val flow = updates[id]
                if (flow == null) {
                    updates[id] = MutableStateFlow(false)
                } else {
                    scope.launch {
                        flow.emit(false)
                    }
                }
            }
        }
    })
    cp.initialize()
    cp.start()
    cp.search()
}

private fun getPanasonicId(device: Device): String = device.udn.split("-").last()
